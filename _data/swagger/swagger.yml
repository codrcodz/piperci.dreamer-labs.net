openapi: '3.0.1'
info:
  description: Piedpiper Gman API
  title: Gman
  version: 1.0.0
paths:
  /task:
    post:
      description: Creates a new task
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateTask'
      responses:
        '200':
          description: New created task event
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TaskEvent'
        '422':
          description: Input Error
        '500':
          description: Internal Server Error
  '/task/{task_id}':
    put:
      description: Creates new task event
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created task
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateEvent'
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TaskEvent'
        '404':
          description: Not Found Error
        '422':
          description: Input Error
        '500':
          description: Internal Server Error
    head:
      description: Return a header for a task
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/task_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return a task
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Task'
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/task/{task_id}/events':
    head:
      description: Returns a header for a task's events
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/task_event_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Returns one to many task events
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/TaskEvent'
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/run/{run_id}':
    head:
      description: Returns a task header by run id
      parameters:
        - in: path
          name: run_id
          type: string
          description: run id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/thread_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Returns one to many tasks by run id
      parameters:
        - in: path
          name: run_id
          type: string
          description: run id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Task'
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/run/{run_id}/events':
    head:
      description: Returns a task events header by run id
      parameters:
        - in: path
          name: run_id
          type: string
          description: run id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/thread_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Retruns one to many task events by run id
      parameters:
        - in: path
          name: run_id
          type: string
          description: run id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/TaskEvent'
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/thread/{thread_id}':
    head:
      description: Returns a thread's header
      parameters:
        - in: path
          name: thread_id
          type: uuid
          description: thread id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/thread_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Returns a thread of one to many tasks
      parameters:
        - in: path
          name: thread_id
          type: uuid
          description: thread id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Task'
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/thread/{thread_id}/events':
    head:
      description: Returns a event's header
      parameters:
        - in: path
          name: thread_id
          type: uuid
          description: thread id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/thread_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return a thread of one to many task events
      parameters:
        - in: path
          name: thread_id
          type: uuid
          description: thread id for a created task or thread
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/TaskEvent'
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  /artifact:
    post:
      description: Creates a new artifact
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateArtifact'
      responses:
        '200':
          description: New created artifact
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Artifact'
        '422':
          description: Input Error
        '500':
          description: Internal Server Error
  '/artifact/{artifact}':
    head:
      description: Return an artifact header
      parameters:
        - in: path
          name: artifact
          type: uuid
          description: artifact id for a created artifact
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/artifact_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return an artifact
      parameters:
        - in: path
          name: artifact
          type: uuid
          description: artifact id for a created artifact
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                items: ArtifactSchema
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/artifact/{task_id}':
    head:
      description: Return an artifact header by task id
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created artifact
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/artifact_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return an artifact by task id
      parameters:
        - in: path
          name: task_id
          type: uuid
          description: task id for a created artifact
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                items: ArtifactSchema
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/artifact/{sri}':
    head:
      description: Return an artifact header by SRI
      parameters:
        - in: path
          name: sri
          type: hash
          description: sri hash for a created artifact
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/artifact_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return an artifact by SRI
      parameters:
        - in: path
          name: sri
          type: hash
          description: sri hash for a created artifact
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                items: ArtifactSchema
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/artifact/thread/{thread_id}':
    head:
      description: Return an artifact header by thread id
      parameters:
        - in: path
          name: thread_id
          type: uuid
          description: thread id for a task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/artifact_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return an artifact by thread id
      parameters:
        - in: path
          name: thread_id
          type: uuid
          description: thread id for a task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                items: ArtifactSchema
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
  '/artifact/run/{run_id}':
    head:
      description: Return an artifact header by run id
      parameters:
        - in: path
          name: run_id
          type: string
          description: run id for a task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/artifact_header'
        '400':
          description: Bad Request
        '404':
          description: Zero Results
        '500':
          description: Internal Server Error
    get:
      description: Return an artifact by run id
      parameters:
        - in: path
          name: run_id
          type: string
          description: run id for a task
          required: true
      responses:
        '200':
          description: Object to be returned
          content:
            application/json:
              schema:
                items: ArtifactSchema
        '404':
          description: Not Found Error
        '500':
          description: Internal Server Error
components:
  schemas:
    Task:
      type: object
      properties:
        project:
          type: string
        task_id:
          type: string
        thread_id:
          type: string
          nullable: true
        parent_id:
          type: string
          nullable: true
        caller:
          type: string
        run_id:
          type: string
      required:
        - caller
        - project
        - run_id
    TaskEvent:
      type: object
      properties:
        timestamp:
          type: string
          format: date-time
        status:
          type: string
          enum:
            - started
            - completed
            - failed
            - delegated
            - received
            - info
        event_id:
          type: string
        return_code:
          type: integer
          format: int32
          nullable: true
        message:
          type: string
        task:
          $ref: '#/components/schemas/Task'
      required:
        - message
        - status
        - timestamp
    CreateEvent:
      type: object
      properties:
        message:
          type: string
        thread_id:
          type: string
          nullable: true
        status:
          type: string
          enum:
            - failed
            - delegated
            - info
            - completed
      required:
        - message
        - status
    CreateTask:
      type: object
      properties:
        project:
          type: string
        status:
          type: string
          enum:
            - started
            - received
        thread_id:
          type: string
          nullable: true
        parent_id:
          type: string
          nullable: true
        message:
          type: string
        caller:
          type: string
        run_id:
          type: string
      required:
        - caller
        - message
        - project
        - run_id
        - status
    task_header:
      type: object
      properties:
        x_gman_task_state:
          type: string
          description: Current state of the specified task
    task_event_header:
      type: object
      properties:
        x-gman-events:
          type: integer
          description: Number of events for the specified task
    thread_header:
      type: object
      properties:
        x-gman-tasks-running:
          type: integer
          description: Number of running tasks
        x-gman-tasks-completed:
          type: integer
          description: Number of completed tasks
        x-gman-tasks-pending:
          type: integer
          description: Number of pending tasks
        x-gman-tasks-failed:
          type: integer
          description: Number of failed tasks
    artifact_header:
      type: object
      properties:
        x-gman-artifact-status:
          type: string
          description: Status of specified artifact
    CreateArtifact:
      type: object
      properties:
        type:
          type: string
          enum:
            - log
            - container
            - artifact
            - source
        task_id:
          type: string
        sri:
          type: string
        uri:
          type: string
      required:
        - sri
        - task_id
        - type
        - uri
    Artifact:
      type: object
      properties:
        type:
          type: string
          enum:
            - log
            - container
            - artifact
            - source
        sri:
          type: string
        status:
          type: string
          enum:
            - unique
            - collision
            - deleted
            - unknown
        event_id:
          type: string
        artifact_id:
          type: string
        uri:
          type: string
        task:
          $ref: '#/components/schemas/Task'
      required:
        - event_id
        - sri
        - status
        - type
        - uri
