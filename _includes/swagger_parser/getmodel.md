{% assign required = site.data.swagger[page.swaggerfile]components.schemas[page.swaggerkey]required %}

{% assign ref = "$ref" %}
<table>
  <thead>
    <tr><th>Name</th><th>Type</th><th>Description</th><th> </th></tr>
  </thead>
  <tbody>
    {% for property in site.data.swagger[page.swaggerfile]components.schemas[page.swaggerkey]properties %}
      {% if property[0] == 'id' %}
      {% else %}
      <tr>    
        <td><code>{{ property[0] }}</code></td>
        <td>  
          {% if property[1].[ref] %}
            <code>model&lt;{{ property[1].[ref] | remove_first:'#/components/schemas/' }}&gt;</code>
          {% else %}
            <code>{{ property[1].type }}</code>
            {% if property[1].format %}
              <code>&lt;{{ property[1].format }}&gt;</code>
            {% endif %}
          {% endif %}
        </td>
        <td>
          {% if property[1].enum %}
             Values:  <br>
            {% for value in property[1].enum %}
              <code>{{ value }}</code>,<br>
            {% endfor %}
          {% endif %}
          {{ property[1].description }}
        </td>
        <td>
          {% if property[1].readOnly == true %}
            READ<br>ONLY
          {% endif %}
          {% if include.required == "yes" %}
            {% for reqprop in required %}
              {% if reqprop == property[0] %}
                REQUIRED
              {% endif %}
            {% endfor %}
          {% endif %}  
        </td>
      </tr>
      {% endif %}
    {% endfor %}
  </tbody>
</table>
