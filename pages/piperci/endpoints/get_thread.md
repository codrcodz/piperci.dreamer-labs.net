---
title: GET /thread/{thread_id}
sidebar: mydoc_sidebar
permalink: get_thread.html
folder: piperci
swaggerfile: swagger
swaggerkey: /thread/{thread_id}
method: get
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_task.html" many="yes" model= "Tasks" %}
