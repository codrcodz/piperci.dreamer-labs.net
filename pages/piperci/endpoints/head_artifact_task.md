---
title: HEAD /artifact/{task_id}
sidebar: mydoc_sidebar
permalink: /head_artifact_task.html
folder: piperci
swaggerfile: swagger
swaggerkey: /artifact/{task_id}
method: head
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_artifact_header.html" model= "Artifact Header" %}
