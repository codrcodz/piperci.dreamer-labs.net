---
title: HEAD /task/{task_id}
sidebar: mydoc_sidebar
permalink: head_task.html
folder: piperci
swaggerfile: swagger
swaggerkey: /task/{task_id}
method: head
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_task_header.html" model= "Task Header" %}
