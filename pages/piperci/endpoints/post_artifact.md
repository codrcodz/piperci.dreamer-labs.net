---
title: POST /artifact
sidebar: mydoc_sidebar
permalink: artifact.html
folder: piperci
swaggerfile: swagger
swaggerkey: /artifact
method: post
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}

## Body parameters
{% include swagger_parser/getrequestbody.md %}

## Examples
<pre><code>{
  "type": "artifact",
  "task_id": "ccf3c6be-279c-4b9e-ac9f-a0a443965738",
  "sri": "sha256-ohZFf987YuVgVs4E9bBjjVOo0VFozHIeYwpfmTzgUYE=",
  "uri": "https://someminio.example.com/art1"
}
</code></pre>

## Responses
{% include swagger_parser/getresponses.md path="/api_artifact.html" model= "Artifact"%}
