---
title: GET /task/{task_id}/events
sidebar: mydoc_sidebar
permalink: get_task_event.html
folder: piperci
swaggerfile: swagger
swaggerkey: /task/{task_id}/events
method: get
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_task_event.html" many="yes" model= "Task Events"%}
