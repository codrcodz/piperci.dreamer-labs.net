---
title: GET /run/{run_id}
sidebar: mydoc_sidebar
permalink: get_run.html
folder: piperci
swaggerfile: swagger
swaggerkey: /run/{run_id}
method: get
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}
## Path parameters
{% include swagger_parser/getparams.md paramtype="path" %}
## Responses
{% include swagger_parser/getresponses.md path="/api_task.html" many="yes" model= "Tasks" %}
